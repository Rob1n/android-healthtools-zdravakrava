package hr.shms.zdravakravaalati.Util;


public class Constants {

    public static String PREFS_KEY = "app_settings";

    public static String BUGSENSE_APIKEY = "86ae95ed";

    public static String PARSE_APP_ID = "TJCMx4jB69exqlnYIqRYkaPx6XJ3FVHYkIuzf8FZ";
    public static String PARSE_CLIENT_KEY = "3rosmlClAygvwNlbWHLd81jci1Of9bCWkR37wMU6";

    public static String GOOGLE_TRACKING_ID = "UA-46685709-6";

    /*  SHARED PREFERENCES VALUES
        boolean WizardFinished
        boolean NotificationsOn
        String  language
        String  measurementSystem
        String  gender
        long    height
        long    weight
        int     age
        int     wrist
        int     waist
        int     hips
        long    bmi
        long    ibw
        long    aibw
    */

    public static String ZDRAVA_KRAVA_URL = "http://zdravakrava.hr";

    // unit conversion
    public static double CM_TO_INCH = 0.3937;
    public static double KG_TO_POUND = 2.2046;

    // for color calculations
    public static int BMI_MIN_VALUE = 15;
    public static int BMI_MAX_VALUE = 40;

    // scroller listView intervals
    public static int HEIGHT_MIN_CM = 150;
    public static int HEIGHT_MAX_CM = 250;
    public static int HEIGHT_MIN_INCH = 60;
    public static int HEIGHT_MAX_INCH = 100;

    public static int WEIGHT_MIN_KG = 50;
    public static int WEIGHT_MAX_KG = 200;
    public static int WEIGHT_MIN_LB = 100;
    public static int WEIGHT_MAX_LB = 600;

    public static int WRIST_MIN_CM = 2;
    public static int WRIST_MAX_CM = 25;
    public static int WRIST_MIN_INCH = 1;
    public static int WRIST_MAX_INCH = 20;

    public static int WAIST_MIN_CM = 30;
    public static int WAIST_MAX_CM = 150;
    public static int WAIST_MIN_INCH = 20;
    public static int WAIST_MAX_INCH = 80;

    public static int HIPS_MIN_CM = 30;
    public static int HIPS_MAX_CM = 200;
    public static int HIPS_MIN_INCH = 30;
    public static int HIPS_MAX_INCH = 150;

    // default value (default scroller position)
    public static int AGE_DEFAULT = 20;
    public static int HEIGHT_DEFAULT_CM = (HEIGHT_MIN_CM+HEIGHT_MAX_CM)/2;
    public static int WEIGHT_DEFAULT_KG = (WEIGHT_MIN_KG+WEIGHT_MAX_KG)/2;
    public static int WRIST_DEFAULT_CM = (WRIST_MIN_CM+WRIST_MAX_CM)/2;
    public static int WAIST_DEFAULT_CM = (WAIST_MIN_CM+WAIST_MAX_CM)/2;
    public static int HIPS_DEFAULT_CM = (HIPS_MIN_CM+HIPS_MAX_CM)/2;

    public static int HEIGHT_DEFAULT_INCH = (HEIGHT_MIN_INCH+HEIGHT_MAX_INCH)/2;
    public static int WEIGHT_DEFAULT_LB = (WEIGHT_MIN_LB+WEIGHT_MAX_LB)/2;
    public static int WRIST_DEFAULT_INCH = (WRIST_MIN_INCH+WRIST_MAX_INCH)/2;
    public static int WAIST_DEFAULT_INCH = (WAIST_MIN_INCH+WAIST_MAX_INCH)/2;
    public static int HIPS_DEFAULT_INCH = (HIPS_MIN_INCH+HIPS_MAX_INCH)/2;

}
