package hr.shms.zdravakravaalati.Util;

import java.math.BigDecimal;
import java.math.RoundingMode;


public class Functions {

    public static double calculateMetricBMI(double visina, double tezina){
        return (tezina / ((visina / 100) * (visina / 100)));
    }

    public static double calculateImperialBMI(double visina, double tezina){
        return ((tezina / (visina*visina))*703);
    }

    public static boolean isNumeric(String str) {
        try
        {
            double d = Double.parseDouble(str);
        }
        catch(NumberFormatException nfe)
        {
            return false;
        }
        return true;
    }

    /**
     * rounds a double to double limited to n decimal places
     * @param value number to be rounded
     * @param places to how many decimal places
     * @return number rounded to that many places. it's pretty straightforward.
     */
    public static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

    public static String cmToImperial (int cm){
        int inches = (int) (cm * Constants.CM_TO_INCH);
        int feet = inches/12;
        inches -= feet*12;

        return Integer.toString(feet)+"\'"+Integer.toString(inches)+"\"";
    }

    /**
     * converts inches to feet'inch" formatted string
     * @param inch main value
     * @return feet'inch" formatted string
     */
    public static String inchToImperial (int inch){
        int feet = inch/12;
        inch -= feet*12;
        return Integer.toString(feet)+"\'"+Integer.toString(inch)+"\"";
    }

    public static int roundDoubleToInt(double d){
        double dAbs = Math.abs(d);
        int i = (int) dAbs;
        double result = dAbs - (double) i;
        if(result<0.5){
            return d<0 ? -i : i;
        }else{
            return d<0 ? -(i+1) : i+1;
        }
    }


}
