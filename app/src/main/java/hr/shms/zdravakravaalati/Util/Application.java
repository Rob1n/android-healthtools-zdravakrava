package hr.shms.zdravakravaalati.Util;

//bugsense
import android.content.SharedPreferences;

//parse
import com.parse.Parse;
import com.parse.PushService;
//google analytics
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;

import hr.shms.zdravakravaalati.Activities.MainActivity;
import hr.shms.zdravakravaalati.Entities.UserClass;


public class Application extends android.app.Application {

    private static Application mInstance;

    private static SharedPreferences sharedPreferences;
    private static double screenDensity;
    private static UserClass korisnik;

    private Tracker mTracker;

    @Override
    public void onCreate() {
        super.onCreate();
        if (mInstance == null) mInstance = Application.this;

        // TODO remove system.out before finishing
//        System.out.println("APPLICATION STARTED");
        LogVM.i("Application", "APPLICATION STARTED");
        sharedPreferences = getSharedPreferences(Constants.PREFS_KEY, 0);
        screenDensity = getResources().getDisplayMetrics().density;
        korisnik = new UserClass(this);

        // Initialize the Parse SDK.
        // https://parse.com/tutorials/get-started-with-android
        Parse.initialize(this, Constants.PARSE_APP_ID, Constants.PARSE_CLIENT_KEY);


        // Initialize BugSense
        // https://www.bugsense.com/docs/android
        //BugSenseHandler.initAndStartSession(this, Constants.BUGSENSE_APIKEY);


        // Google Analytics
        // https://developer.android.com/reference/com/google/android/gms/analytics/Tracker.html
        GoogleAnalytics analytics = GoogleAnalytics.getInstance(this);
        mTracker = analytics.newTracker(Constants.GOOGLE_TRACKING_ID);
        // TODO postaviti trackere?

    }

    @Override
    public void onTerminate() {
        super.onTerminate();
    }


    public static Application getmInstance() {
        return mInstance;
    }

    public Tracker getAnalyticsTracker() {
        return mTracker;
    }

    /**
     * Toggles Parse push notifications based on user preferences, activated from action bar settings
     * @param turnedOn true if notifications are enabled
     */
    public void toggleParsePushNotifications(boolean turnedOn){
        if (turnedOn)
            PushService.setDefaultPushCallback(this, MainActivity.class);
        else
            PushService.setDefaultPushCallback(this, null);
    }

    public SharedPreferences getSharedPreferencesFromApp() {
        return sharedPreferences;
    }

    public UserClass getKorisnik() {
        return korisnik;
    }

    public double getScreenDensity() {
        return screenDensity;
    }
}
