package hr.shms.zdravakravaalati.Activities;

import android.app.Activity;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Point;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.ViewDragHelper;


import com.actionbarsherlock.app.SherlockActivity;

import java.lang.reflect.Field;
import java.util.Locale;

import hr.shms.zdravakravaalati.Enums.Language;
import hr.shms.zdravakravaalati.Util.Application;


public class BaseActivity extends SherlockActivity {

    SharedPreferences sharedPreferences;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        sharedPreferences = Application.getmInstance().getSharedPreferencesFromApp();

        // are parse notifications on or off?
        boolean notifications = sharedPreferences.getBoolean("NotificationsOn", true);
        Application.getmInstance().toggleParsePushNotifications(notifications);

        // initiate chosen language
        Locale locale = null;
        Language language = Language.toMyEnum(sharedPreferences.getString("language", Language.ENGLISH.toString()));
        switch (language){
            case ENGLISH:
                locale = new Locale("en");
                break;
            case CROATIAN:
                locale = new Locale("hr");
                break;
        }
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getResources().updateConfiguration(config, null);


    }



    /**
     * Increases the margin by which we can slide in the navigation drawer from the left edge
     * @param activity The activitiy of the navigation drawer (MainActivity)
     * @param drawerLayout DrawerLayout on which it's applied
     * @param displayWidthPercentage New margin size in screen width percentage
     */
    public static void setDrawerLeftEdgeSize(Activity activity, DrawerLayout drawerLayout, float displayWidthPercentage) {
        if (activity == null || drawerLayout == null)
            return;

        try {
            // find ViewDragHelper and set it accessible
            Field leftDraggerField = drawerLayout.getClass().getDeclaredField("mLeftDragger");
            leftDraggerField.setAccessible(true);
            ViewDragHelper leftDragger = (ViewDragHelper) leftDraggerField.get(drawerLayout);
            // find edgesize and set is accessible
            Field edgeSizeField = leftDragger.getClass().getDeclaredField("mEdgeSize");
            edgeSizeField.setAccessible(true);
            int edgeSize = edgeSizeField.getInt(leftDragger);
            // set new edgesize
            Point displaySize = new Point();
            activity.getWindowManager().getDefaultDisplay().getSize(displaySize);
            edgeSizeField.setInt(leftDragger, Math.max(edgeSize, (int) (displaySize.x * displayWidthPercentage)));
        } catch (NoSuchFieldException e) {
            // ignore
        } catch (IllegalArgumentException e) {
            // ignore
        } catch (IllegalAccessException e) {
            // ignore
        }
    }

}
