package hr.shms.zdravakravaalati.Activities;


import android.app.Fragment;
import android.app.FragmentManager;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.widget.DrawerLayout;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;

import java.util.ArrayList;
import java.util.Collections;

import hr.shms.zdravakravaalati.Adapters.NavigationDrawerListAdapter;
import hr.shms.zdravakravaalati.Dialogs.LanguageDialog;
import hr.shms.zdravakravaalati.Dialogs.MeasurementSystemDialog;
import hr.shms.zdravakravaalati.Util.LogVM;
import hr.shms.zdravakravaalati.Fragments.Fragment2Bmi;
import hr.shms.zdravakravaalati.Fragments.Fragment3Ibw;
import hr.shms.zdravakravaalati.Fragments.Fragment4BodyShape;
import hr.shms.zdravakravaalati.Fragments.Fragment5BodyBuild;
import hr.shms.zdravakravaalati.Fragments.Fragment6ZdravaKrava;
import hr.shms.zdravakravaalati.Fragments.Setup1Gender;
import hr.shms.zdravakravaalati.R;
import hr.shms.zdravakravaalati.Util.Application;
import hr.shms.zdravakravaalati.Util.Constants;

public class MainActivity extends BaseActivity {

    private DrawerLayout mDrawerLayout;

    private ListView mDrawerList;
    private ActionBarDrawerToggle mDrawerToggle;

    /* //////////////////////////////////
       OVERRIDE METHODS
    */ //////////////////////////////////

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // testing only, it is now cleared with a button in the overview fragment
        //getSharedPreferences(Constants.PREFS_KEY, 0).edit().clear().commit();

        // staring app for the first time automatically starts the wizard - now from the fragment 1 instead
        /*if (!sharedPreferences.getBoolean("WizardFinished", false)){
            Intent wizard = new Intent(this, WizardActivity.class);
            startActivityForResult(wizard, Constants.REQUEST_EXIT);
        }*/

        findViews();
        initNavDrawer(savedInstanceState);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getSupportMenuInflater().inflate(R.menu.main, menu);

        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        // INITIAL SETUP OF THE NOTIFICATION CHECKBOX
        // notifications themselves are turned on or off in the BaseActivity
        boolean checked = sharedPreferences.getBoolean("NotificationsOn", true);
        menu.findItem(R.id.action_notifications).setChecked(checked);

//        boolean drawerOpen = mDrawerLayout.isDrawerOpen(mDrawerList);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
//        if (mDrawerToggle.onOptionsItemSelected(item) { // doesnt work with actionbarsherlock
//            return true;
//        }

        // ACTION BAR SETTINGS ON CLICK EVENTS
        int id = item.getItemId();
        switch (id) {
            case R.id.action_measurement_unit_system:
                new MeasurementSystemDialog().show(getFragmentManager(), "MeasurementSystem");
                return true;
            case R.id.action_feedback:
                Toast.makeText(this, "Example feedback.", Toast.LENGTH_SHORT).show();

                // TODO feedback mail sharedPreferences
                Intent Email = new Intent(Intent.ACTION_SEND);
                Email.setType("text/email");
                Email.putExtra(Intent.EXTRA_EMAIL, new String[]{"admin@gmail.com"});
                Email.putExtra(Intent.EXTRA_SUBJECT, "Feedback");
                Email.putExtra(Intent.EXTRA_TEXT, "Dear ...," + "");
                startActivity(Intent.createChooser(Email, "Send Feedback:"));

                return true;
            case R.id.action_language:
                new LanguageDialog().show(getFragmentManager(), "Language");
                return true;
            case R.id.action_notifications:
                if (item.isChecked()) {
                    item.setChecked(false);
                    sharedPreferences.edit().putBoolean("NotificationsOn", false).commit();
                    Application.getmInstance().toggleParsePushNotifications(false);
                } else {
                    item.setChecked(true);
                    sharedPreferences.edit().putBoolean("NotificationsOn", true).commit();
                    Application.getmInstance().toggleParsePushNotifications(true);
                }
                return true;
            default:
                // clicking the home icon
                if (mDrawerLayout.isDrawerOpen(mDrawerList))
                    mDrawerLayout.closeDrawer(mDrawerList);
                else
                    mDrawerLayout.openDrawer(mDrawerList);

                return super.onOptionsItemSelected(item);
        }
    }


    /* //////////////////////////////////
   PUBLIC METHODS
    */ //////////////////////////////////

    /**
     * Use setItemChecked or setSelection for the currently picked drawer
     *
     * @return Returns the main navigation drawer list
     */
    public ListView getmDrawerList() {
        return mDrawerList;
    }


    /* //////////////////////////////////
       PRIVATE METHODS
    */ //////////////////////////////////

    private void findViews() {
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerList = (ListView) findViewById(R.id.nav_drawer_listview);
    }

    private void initNavDrawer(Bundle mSavedInstanceState) {

        // TODO popraviti slideanje nav drawera sa bilo koje pozicije
        setDrawerLeftEdgeSize(this, mDrawerLayout, 0.5f);

        // POPULATING THE NAVIGATION DRAWER
        String[] navMenuTitles = getResources().getStringArray(R.array.navDrawer_sections);
        ArrayList<String> navDrawerItemsList = new ArrayList<String>();
        // adding all navMenuTitles to the navDrawerItemsList
        Collections.addAll(navDrawerItemsList, navMenuTitles);


        // setting the adapter
        NavigationDrawerListAdapter adapter = new NavigationDrawerListAdapter(getApplicationContext(), navDrawerItemsList);
        mDrawerList.setAdapter(adapter);

        // list's click listener
        mDrawerList.setOnItemClickListener(new NavMenuClickListener());

        // DRAWER TOGGLE CREATING
        mDrawerToggle = new ActionBarDrawerToggle(
                this,
                mDrawerLayout,
                R.drawable.ic_drawer, // nav drawer icon to replace 'Up' caret
                R.string.app_name, // "open drawer" description
                R.string.app_name // "close drawer" description
        ) {
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);

                try {
                    getSupportActionBar().setTitle(getTitle());
                } catch (NullPointerException e) {
                    LogVM.e("MainActivity", "setting title onDrawerOpened");
                }

                invalidateOptionsMenu();
            }

            public void onDrawerOpened(View drawerView) {
                super.onDrawerClosed(drawerView);

                try {
                    getSupportActionBar().setTitle(getTitle());
                } catch (NullPointerException e) {
                    LogVM.e("MainActivity", "setting title onDrawerOpened");
                }
                invalidateOptionsMenu();
            }
        };
        // DRAWER TOGGLE SETTING
        mDrawerLayout.setDrawerListener(mDrawerToggle);

        // SETTING THE ACTION BAR ICON
        try {
            // ENABLING THE ARROW ICON WHICH OPENS THE NAVIGATION DRAWER
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            // TO CHANGE THE ICON RESOURCE - android:logo IN THE MANIFEST - faster
            //getActionBar().setIcon(R.drawable.ic_drawer);
        } catch (java.lang.NullPointerException e) {
            LogVM.e("MainActivity", "NullPointerException kod getActionbar");
        }
        getSupportActionBar().setHomeButtonEnabled(true);

        // SETS THE FIRST SCREEN WHICH WILL BE OPENED
        if (mSavedInstanceState == null) {
            displayView(0); // TODO pocetni ekran baziran na postavkama?
        }


    }

    private class NavMenuClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
            displayView(position);
        }
    }

    /**
     * Starts the appropriate fragment based on the item selected in the navigation drawer
     *
     * @param position index of the item selected in the navigation drawer
     */
    private void displayView(int position) {
        Fragment fragment = null;
        switch (position) {
            case 0:
                fragment = new Setup1Gender();
                break;
            case 1:
                fragment = new Fragment2Bmi();
                break;
            case 2:
                fragment = new Fragment3Ibw();
                break;
            case 3:
                fragment = new Fragment4BodyShape();
                break;
            case 4:
                fragment = new Fragment5BodyBuild();
                break;
            case 5:
                Toast.makeText(getApplicationContext(), getString(R.string.webview_opening), Toast.LENGTH_SHORT).show();

                //openZdravaKrava();
                //TODO rjesi stackoverflow za vanjski browser
                fragment = new Fragment6ZdravaKrava();

                break;
        }

        if (fragment != null) {
            FragmentManager fragmentManager = getFragmentManager();
            // clears (setup's) backstack when switching to another drawer
            fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            fragmentManager.beginTransaction()
                    .replace(R.id.main_container, fragment)
                    .commit();

            mDrawerList.setItemChecked(position, true);
            mDrawerList.setSelection(position);

            // TO SHOW INDIVIDUAL TITLES ON TOP - ENABLE THIS
//            setTitle(navMenuTitles[position]);

            mDrawerLayout.closeDrawer(mDrawerList);
        } else {
            LogVM.e("MainActivity", "Error switching fragments");
        }

    }

    private void openZdravaKrava() {
        try {
            Intent myIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(Constants.ZDRAVA_KRAVA_URL));
            startActivity(myIntent);
        } catch (ActivityNotFoundException e) {
            Toast.makeText(this, getResources().getString(R.string.error_opening_website), Toast.LENGTH_LONG).show();
            LogVM.e("MainActivity", "No browser application");
        }

    }

    /*
    @Override
    public void setTitle(CharSequence title){
        mTitle = title;
        getActionBar().setTitle(mTitle);
    }*/


}
