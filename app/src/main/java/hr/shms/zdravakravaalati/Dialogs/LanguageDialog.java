package hr.shms.zdravakravaalati.Dialogs;

import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.widget.Toast;

import hr.shms.zdravakravaalati.Activities.MainActivity;
import hr.shms.zdravakravaalati.Enums.Language;
import hr.shms.zdravakravaalati.R;
import hr.shms.zdravakravaalati.Util.Application;


public class LanguageDialog extends DialogFragment {
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        builder.setTitle(R.string.dialog_pick_language)
                .setItems(getResources().getStringArray(R.array.language_options), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int which) {

                        SharedPreferences sharedPreferences = Application.getmInstance().getSharedPreferencesFromApp();
                        Language current = Language.toMyEnum(sharedPreferences.getString("language", Language.ENGLISH.toString()));
                        Language selected = null;
                        switch (which){
                            case 0: selected = Language.ENGLISH; break;
                            case 1: selected = Language.CROATIAN; break;
                        }

                        if (current != selected && selected != null){

                            sharedPreferences.edit().putString("language", selected.toString()).apply();

                            // RESTARING APPLICATION
                            Toast.makeText(getActivity(), getString(R.string.restaring_app), Toast.LENGTH_SHORT).show();
                            Intent mStartActivity = new Intent(getActivity(), MainActivity.class);
                            int mPendingIntentId = 123456;
                            PendingIntent mPendingIntent = PendingIntent.getActivity(getActivity(), mPendingIntentId, mStartActivity, PendingIntent.FLAG_CANCEL_CURRENT);
                            AlarmManager mgr = (AlarmManager)getActivity().getSystemService(Context.ALARM_SERVICE);
                            mgr.set(AlarmManager.RTC, System.currentTimeMillis() + 1000, mPendingIntent);
//                            System.exit(0);
                            getActivity().finish();

                        }

                    }
                })
        .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });

        return builder.create();
    }
}
