package hr.shms.zdravakravaalati.Entities;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.widget.TextView;

import hr.shms.zdravakravaalati.Enums.Gender;
import hr.shms.zdravakravaalati.Enums.MeasurementUnit;
import hr.shms.zdravakravaalati.R;
import hr.shms.zdravakravaalati.Util.Application;
import hr.shms.zdravakravaalati.Util.Constants;
import hr.shms.zdravakravaalati.Util.Functions;

public class UserClass {

    // GENERAL
    private SharedPreferences sharedPreferences;
    private Context context;
    private double scale;

    private MeasurementUnit measurementSystem;
    private Gender gender;
    private double height;
    private double weight;
    private int age;
    private int wrist;
    private int waist;
    private int hips;

    private double bmi;
    private double ibw;
    private double aibw;


    /**
     * CONSTRUCTOR - initializes (default if necessary) user parameters
     *
     * @param context Used to getResources (strings)
     */
    public UserClass(Context context) {
        setSharedPreferences(Application.getmInstance().getSharedPreferencesFromApp());
        setScale(Application.getmInstance().getScreenDensity());
        setContext(context);
        reloadData();
    }

    /**
     * used to refresh user data (ie., in case of the data reset)
     */
    public void reloadData() {
        measurementSystem = MeasurementUnit.toMyEnum(sharedPreferences.getString("measurementSystem", MeasurementUnit.METRIC.toString()));
        age = sharedPreferences.getInt("age", Constants.AGE_DEFAULT);
        gender = Gender.toMyEnum(sharedPreferences.getString("gender", Gender.MALE.toString()));
        switch (measurementSystem) {
            case METRIC:
                height = Double.longBitsToDouble(sharedPreferences.getLong("height", Double.doubleToRawLongBits(Constants.HEIGHT_DEFAULT_CM)));
                weight = Double.longBitsToDouble(sharedPreferences.getLong("weight", Double.doubleToRawLongBits(Constants.WEIGHT_DEFAULT_KG)));
                waist = sharedPreferences.getInt("waist", Constants.WAIST_DEFAULT_CM);
                wrist = sharedPreferences.getInt("wrist", Constants.WRIST_DEFAULT_CM);
                hips = sharedPreferences.getInt("hips", Constants.HIPS_DEFAULT_CM);
                break;
            case IMPERIAL:
                height = Double.longBitsToDouble(sharedPreferences.getLong("height", Double.doubleToRawLongBits(Constants.HEIGHT_DEFAULT_INCH)));
                weight = Double.longBitsToDouble(sharedPreferences.getLong("weight", Double.doubleToRawLongBits(Constants.WEIGHT_DEFAULT_LB)));
                waist = sharedPreferences.getInt("waist", Constants.WAIST_DEFAULT_INCH);
                wrist = sharedPreferences.getInt("wrist", Constants.WRIST_DEFAULT_INCH);
                hips = sharedPreferences.getInt("hips", Constants.HIPS_DEFAULT_INCH);
                break;
        }
        calculateBmi();
        calculateIbw();
    }

    public double calculateBmi() {
        double tempBmi = 0;
        switch (getMeasurementSystem()) {
            case METRIC:
                tempBmi = Functions.round(Functions.calculateMetricBMI(getHeight(), getWeight()), 2);
                break;
            case IMPERIAL:
                tempBmi = Functions.round(Functions.calculateImperialBMI(getHeight(), getWeight()), 2);
                break;
        }
        setBmi(tempBmi);
        return tempBmi;
    }

    public double calculateIbw() {

        double result = 0;
        double height = getHeight();

        switch (getMeasurementSystem()) {
            case IMPERIAL:
                // inch to centimeter conversion
                height /= Constants.CM_TO_INCH;
                break;
        }

        height /= 100;
        switch (getGender()) {
            case FEMALE:
                result = (0.4 * getBmi() + 0.03 * getAge() + 11) * (height * height);
                break;
            case MALE:
                result = (0.5 * getBmi() + 11.5) * (height * height);
                break;
        }

        switch (getMeasurementSystem()) {
            case IMPERIAL:
                // kilogram to pound conversion
                result *= Constants.KG_TO_POUND;
                break;
        }

        setIbw(Functions.roundDoubleToInt(result));
        return Functions.roundDoubleToInt(result);
    }

//    /**
//     *<p><b>ABW<br></b>Estimated adjusted body weight (kg)<b><br></b>If the actual
//     body weight is greater than 30% of the calculated&nbsp;IBW, calculate the
//     adjusted body weight (ABW): <b><br>ABW = IBW + 0.4(actual weight - IBW)</b></p>
//     * @return adjusted body weight
//     */
//    public double calculateAIbw(){
//        if (ibw != 0) {
//            double aibw = getIbw() + 0.4 * (getWeight() - getIbw());
//            aibw = Functions.round(aibw, 2);
//            setAibw(aibw);
//            return aibw;
//        }
//        return 0;
//    }

    public String calculateBodyShape() {

        double ratio = getWaist() / getHips();
        double limit = 0;

        switch (getGender()) {
            case MALE:
                limit = 1;
                break;
            case FEMALE:
                limit = 0.8;
                break;
        }

        if (ratio > limit)
            return context.getResources().getString(R.string.apple);
        else
            return context.getResources().getString(R.string.pear);

    }

    public String calculateBodyBuild() {

        double height = getHeight();
        double wrist = getWrist();

        switch (getMeasurementSystem()) {
            case IMPERIAL:
                height /= Constants.CM_TO_INCH;
                wrist /= Constants.CM_TO_INCH;
                break;
        }
        double ratio = height / wrist;

        String result = "";

        switch (getGender()) {
            case FEMALE:
                if (ratio < 10.1) result = context.getResources().getString(R.string.heavy);
                if (ratio >= 10.1 && ratio <= 11)
                    result = context.getResources().getString(R.string.normal);
                if (ratio > 11) result = context.getResources().getString(R.string.light);
                break;
            case MALE:
                if (ratio < 9.6) result = context.getResources().getString(R.string.heavy);
                if (ratio >= 9.6 && ratio <= 10.4)
                    result = context.getResources().getString(R.string.normal);
                if (ratio > 10.4) result = context.getResources().getString(R.string.light);
                break;
        }

        return result;

    }

    /**
     * Formats the current BMI so it's suitable to use in TextViews
     *
     * @return String variable that ranges from "<BMI_MIN_VALUE" to ">BMI_MAX_VALUE"
     */
    public String getBmiText() {
        int min_value = Constants.BMI_MIN_VALUE;
        int max_value = Constants.BMI_MAX_VALUE;

        String rez;
        if (bmi < min_value) {
            bmi = min_value;
            rez = "<" + Functions.roundDoubleToInt(bmi);
        } else if (bmi > max_value) {
            bmi = max_value;
            rez = ">" + Functions.roundDoubleToInt(bmi);
        } else {
            rez = Double.toString(Functions.round(bmi, 2));
        }

        return rez;
    }

    /**
     * Sets the BMI textView with current BMI and also updates the color of it's background
     *
     * @param bmiTextView On which this will be applied - requires the appropriate Drawable background
     */
    public void setBmiColor(TextView bmiTextView) {
        // SETTING THE COLOR
        GradientDrawable drawable = (GradientDrawable) bmiTextView.getBackground();
        drawable.setColor(getBmiColor());
    }

    /**
     * Calculates the RGB color depending on the current BMI value, shades between blue (low), green (normal) and red (high)
     *
     * @return RGB color in the integer format, can be used with drawable.setColor()
     */
    public int getBmiColor() {
        int min_value = Constants.BMI_MIN_VALUE;
        int max_value = Constants.BMI_MAX_VALUE;

        // SCALING BMI TO VALUE BETWEEN 0 AND 100      +*ADJUSTMENT because ideal BMI is not at the precise center
        double percentage = (((getBmi() - min_value) / (max_value - min_value)) + 0.2) * 1.2;
        if (percentage > 1) percentage = 1;
        // CALCULATING THE COLOR BASED ON BMI (PERCENTAGE) VALUE
        int red, green, blue;
        if (percentage < 0.25) {
            red = 0;
            green = (int) (percentage * 255 * 4);
            blue = 255;
        } else if (percentage >= 0.25 && percentage < 0.5) {
            red = 0;
            green = 255;
            blue = (int) (255 - (((percentage - 0.25) * 4) * 255));
        } else if (percentage >= 0.5 && percentage < 0.75) {
            red = (int) (((percentage - 0.5) * 4) * 255);
            green = 255;
            blue = 0;
        } else {
            red = 255;
            green = (int) (255 - (((percentage - 0.75) * 4) * 255));
            blue = 0;
        }

        return Color.rgb(red, green, blue);
    }

    /**
     * Calculates the BMI category of the user based on BMI range
     * <table class="wikitable">
     * <tbody><tr>
     * <th>Category</th>
     * <th>BMI range – kg/m<sup>2</sup></th>
     * <th>BMI Prime</th>
     * </tr>
     * <tr>
     * <td>Very severely underweight</td>
     * <td>less than 15</td>
     * <td>less than 0.60</td>
     * </tr>
     * <tr>
     * <td>Severely underweight</td>
     * <td>from 15.0 to 16.0</td>
     * <td>from 0.60 to 0.64</td>
     * </tr>
     * <tr>
     * <td>Underweight</td>
     * <td>from 16.0 to 18.5</td>
     * <td>from 0.64 to 0.74</td>
     * </tr>
     * <tr>
     * <td>Normal (healthy weight)</td>
     * <td>from 18.5 to 25</td>
     * <td>from 0.74 to 1.0</td>
     * </tr>
     * <tr>
     * <td>Overweight</td>
     * <td>from 25 to 30</td>
     * <td>from 1.0 to 1.2</td>
     * </tr>
     * <tr>
     * <td>Obese Class I (Moderately obese)</td>
     * <td>from 30 to 35</td>
     * <td>from 1.2 to 1.4</td>
     * </tr>
     * <tr>
     * <td>Obese Class II (Severely obese)</td>
     * <td>from 35 to 40</td>
     * <td>from 1.4 to 1.6</td>
     * </tr>
     * <tr>
     * <td>Obese Class III (Very severely obese)</td>
     * <td>over 40</td>
     * <td>over 1.6</td>
     * </tr>
     * </tbody></table>
     *
     * @return returns the index of class in which the user is
     */
    public int getBmiClass() {
        if (bmi < 15) return 1;
        if (bmi < 16) return 2;
        if (bmi < 18.5) return 3;
        if (bmi < 25) return 4;
        if (bmi < 30) return 5;
        if (bmi < 35) return 6;
        if (bmi < 40) return 7;
        return 8;
    }


    /* ////////////////////////////////
        GETTERS AND SETTERS
    */ ////////////////////////////////


    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        if (height != this.height) {
            this.height = height;
            sharedPreferences.edit().putLong("height", Double.doubleToRawLongBits(height)).apply();
        }
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        if (weight != this.weight) {
            this.weight = weight;
            sharedPreferences.edit().putLong("weight", Double.doubleToRawLongBits(weight)).apply();
        }
    }

    public double getBmi() {
        return bmi;
    }

    public void setBmi(double bmi) {
        this.bmi = bmi;
        sharedPreferences.edit().putLong("bmi", Double.doubleToRawLongBits(bmi)).apply();
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
        sharedPreferences.edit().putInt("age", age).apply();
    }

    public Gender getGender() {
        return this.gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
        sharedPreferences.edit().putString("gender", gender.toString()).apply();
    }

    public int getWrist() {
        return wrist;
    }

    public void setWrist(int wrist) {
        if (wrist != this.wrist) {
            this.wrist = wrist;
            sharedPreferences.edit().putInt("wrist", wrist).apply();
        }
    }

    public int getWaist() {
        return waist;
    }

    public void setWaist(int waist) {
        if (waist != this.waist) {
            this.waist = waist;
            sharedPreferences.edit().putInt("waist", waist).apply();
        }
    }

    public int getHips() {
        return hips;
    }

    public void setHips(int hips) {
        if (hips != this.hips) {
            this.hips = hips;
            sharedPreferences.edit().putInt("hips", hips).apply();
        }
    }

    public MeasurementUnit getMeasurementSystem() {
        return measurementSystem;
    }

    public void setMeasurementSystem(MeasurementUnit measurementSystem) {
        this.measurementSystem = measurementSystem;
        sharedPreferences.edit().putString("measurementSystem", measurementSystem.toString()).apply();
    }

    public double getIbw() {
        return ibw;
    }

    public void setIbw(double ibw) {
        sharedPreferences.edit().putLong("ibw", Double.doubleToRawLongBits(ibw));
        this.ibw = ibw;
    }

    public double getAibw() {
        return aibw;
    }

    public void setAibw(double aibw) {
        sharedPreferences.edit().putLong("aibw", Double.doubleToRawLongBits(aibw));
        this.aibw = aibw;
    }

    public double getScale() {
        return scale;
    }

    public void setScale(double scale) {
        this.scale = scale;
    }

    public void setSharedPreferences(SharedPreferences sharedPreferences) {
        this.sharedPreferences = sharedPreferences;
    }

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }
}
