package hr.shms.zdravakravaalati.Entities;


public class ItemOverviewListView {

    private int Id;
    private String name;
    private double value;
    private String mUnit;
    private String stringValue;
    private boolean showImage;

    /**
     * Send clear names, formatting happens in the adapter
     * @param Id BMI - 0, IBW - 1, BB - 2, BS - 3
     * @param name This will be shown in the listview
     * @param value value that will be shown
     * @param showImage true if you want to display the icon to the right of the parameter
     */
    public ItemOverviewListView(int Id, String name, double value, boolean showImage){
        setId(Id);
        setName(name);
        setValue(value);
        setShowImage(showImage);
    }

    /**
     * Send clear names, formatting happens in the adapter
     * @param Id BMI - 0, IBW - 1, BB - 2, BS - 3
     * @param name This will be shown in the listview
     * @param value value that will be shown
     * @param mUnit This will be shown after the value
     * @param showImage true if you want to display the icon to the right of the parameter
     */
    public ItemOverviewListView(int Id, String name, double value, String mUnit, boolean showImage){
        setId(Id);
        setName(name);
        setValue(value);
        setmUnit(mUnit);
        setShowImage(showImage);
    }

    /**
     * For items that don't have numerical values - they print strings out instead
     * @param Id BMI - 0, IBW - 1, BB - 2, BS - 3
     * @param name Parameter name shown in the listiview
     * @param value String value shown after the parameter name
     * @param showImage true if you want to display the icon to the right of the parameter
     */
    public ItemOverviewListView(int Id, String name, String value, boolean showImage){
        setId(Id);
        setName(name);
        setStringValue(value);
        setShowImage(showImage);
    }


    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public String getmUnit() {
        return mUnit;
    }

    public void setmUnit(String mUnit) {
        this.mUnit = mUnit;
    }

    public String getStringValue() {
        return stringValue;
    }

    public void setStringValue(String stringValue) {
        this.stringValue = stringValue;
    }

    public boolean isShowImage() {
        return showImage;
    }

    public void setShowImage(boolean showImage) {
        this.showImage = showImage;
    }
}
