package hr.shms.zdravakravaalati.Fragments;


import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import hr.shms.zdravakravaalati.R;
import hr.shms.zdravakravaalati.Util.Constants;

public class Fragment6ZdravaKrava extends Fragment {

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_6_zdrava_krava, container, false);

        String currentURL = Constants.ZDRAVA_KRAVA_URL;
        if (currentURL != null) {
            WebView wv = (WebView) view.findViewById(R.id.zk_webView);
            // TODO check security flaw, javascript enabled?
            wv.getSettings().setJavaScriptEnabled(false);
            wv.setWebViewClient(new ZkWebViewClient());
            wv.loadUrl(currentURL);
        }

        return view;
    }


    private class ZkWebViewClient extends WebViewClient {

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            return false;
        }


    }

}
