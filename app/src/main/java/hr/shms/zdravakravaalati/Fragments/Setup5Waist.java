package hr.shms.zdravakravaalati.Fragments;

import android.app.Fragment;
import android.app.FragmentManager;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

import hr.shms.zdravakravaalati.Adapters.HeightListAdapter;
import hr.shms.zdravakravaalati.Entities.UserClass;
import hr.shms.zdravakravaalati.R;
import hr.shms.zdravakravaalati.Util.Application;
import hr.shms.zdravakravaalati.Util.Constants;
import hr.shms.zdravakravaalati.Util.Functions;


public class Setup5Waist extends Fragment {
    public Setup5Waist() {}

    private UserClass korisnik;

    private TextView waistTextView;
    private ListView waistListView;
    private TextView measurementUnitTextView;

    private int MIN_VALUE, MAX_VALUE;

    private double waist;
    private int currentWaist;

    /* ////////////////////////////////
        OVERRIDE METHODS
    */ ////////////////////////////////

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.setup5_waist, container, false);
        korisnik = Application.getmInstance().getKorisnik();

        switch (korisnik.getMeasurementSystem()){
            case METRIC:
                MIN_VALUE = Constants.WAIST_MIN_CM;
                MAX_VALUE = Constants.WAIST_MAX_CM;
                break;
            case IMPERIAL:
                MIN_VALUE = Constants.WAIST_MIN_INCH;
                MAX_VALUE = Constants.WAIST_MAX_INCH;
                break;
        }

        // INIT VIEWS
        ImageView stickmanImageView = (ImageView) view.findViewById(R.id.imageView_setup5_stickman);
        ImageView btn_next = (ImageView) view.findViewById(R.id.btn_setup5_next);
        waistTextView = (TextView) view.findViewById(R.id.setup5_counter_textView);
        waistListView = (ListView) view.findViewById(R.id.setup5_listView);
        measurementUnitTextView = (TextView) view.findViewById(R.id.setup5_mu_textView);

        // ON CLICK LISTENERS
        btn_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //setting data
                korisnik.setWaist(currentWaist);

                Fragment fragment = new Setup6Hips();
                FragmentManager fragmentManager = getFragmentManager();
                fragmentManager.beginTransaction()
                        .setCustomAnimations(R.animator.card_flip_right_in, R.animator.card_flip_right_out,
                                R.animator.card_flip_left_in, R.animator.card_flip_left_out)
                        .replace(R.id.main_container, fragment)
                        .addToBackStack(null)
                        .commit();
            }
        });

        initScroller();

        switch (korisnik.getGender()){
            case MALE: stickmanImageView.setImageResource(R.drawable.stickman); break;
            case FEMALE: stickmanImageView.setImageResource(R.drawable.stickwoman); break;
        }

        return view;
    }

    @Override
    public void onDetach() {
        korisnik.setWaist(currentWaist);
        super.onDetach();
    }

    /* ////////////////////////////////
        PRIVATE METHODS
    */ ////////////////////////////////

    private void initScroller(){
        // LISTVIEW INITIALIZATION
        ArrayList<Integer> verDataList = new ArrayList<Integer>();
        for (int i= MIN_VALUE; i<MAX_VALUE; i++)
            verDataList.add(i);
        HeightListAdapter heightListAdapter = new HeightListAdapter(getActivity(), 0,  verDataList);
        waistListView.setAdapter(heightListAdapter);

        // MEASUREMENT UNIT
        switch (korisnik.getMeasurementSystem()){
            case METRIC: measurementUnitTextView.setText("cm"); break;
            case IMPERIAL: measurementUnitTextView.setVisibility(View.GONE); break;
        }

        setupWeightDetection(waistListView, korisnik.getWaist());
    }


    /**
     * Sets up the staring position and onScrollListeners for the vertical listview
     * @param verListView The view to which we're applying onScrollListener
     * @param visina The initial value which will be shown on the list
     */
    private void setupWeightDetection(final ListView verListView, final double visina){
        verListView.post(new Runnable() {
            @Override
            public void run() {
                // CALCULATING THE INITIAL POSITION IN THE LIST
                int visibleItems = verListView.getChildCount();
                int vis_pos = Functions.roundDoubleToInt(visina) - MIN_VALUE - visibleItems / 2;

                // SCROLLING TO THE CALCULATED POSITION
                int correction = 0;
                if (visibleItems%2 == 0)
                    correction = 1;
                smoothScrollToPositionFromTop(verListView, vis_pos+correction);

                // SETTING THE ON SCROLL LISTENER FOR WEIGHT
                verListView.setOnScrollListener(new AbsListView.OnScrollListener() {
                    @Override
                    public void onScrollStateChanged(AbsListView absListView, int scrollState) {}

                    @Override
                    public void onScroll(AbsListView absListView, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                        if (absListView.equals(verListView)) {

                            waist = firstVisibleItem + visibleItemCount / 2 + MIN_VALUE;
//                            korisnik.updateHeightStep1(waist);
                            switch (korisnik.getMeasurementSystem()){
                                case METRIC: waistTextView.setText(Integer.toString(Functions.roundDoubleToInt(waist))); break;
                                case IMPERIAL: waistTextView.setText(Integer.toString(Functions.roundDoubleToInt(waist))+"\""); break;
                            }
//                            korisnik.setWaist(Functions.roundDoubleToInt(waist));
                            currentWaist = Functions.roundDoubleToInt(waist);
                        }
                    }
                });

            }
        });
    }

    /**
     * A custom scrollToPosition method which adjusts the offset caused by the default smoothScroll <br/>
     * source: https://stackoverflow.com/questions/14479078/smoothscrolltopositionfromtop-is-not-always-working-like-it-should/20997828#20997828
     * @param view the listView which is going to be scrolled
     * @param position list index to which we're scrolling
     */
    private static void smoothScrollToPositionFromTop(final AbsListView view, final int position) {
        View child = getChildAtPosition(view, position);
        // There's no need to scroll if child is already at top or view is already scrolled to its end
        if ((child != null) && ((child.getTop() == 0) || ((child.getTop() > 0) && !view.canScrollVertically(1)))) {
            return;
        }

        view.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(final AbsListView view, final int scrollState) {
                if (scrollState == SCROLL_STATE_IDLE) {
                    view.setOnScrollListener(null);

                    // Fix for scrolling bug
                    new Handler().post(new Runnable() {
                        @Override
                        public void run() {
                            view.setSelection(position);
                        }
                    });
                }
//                System.out.println("customOnScrollStateChanged listener");
            }

            @Override
            public void onScroll(final AbsListView view, final int firstVisibleItem, final int visibleItemCount,
                                 final int totalItemCount) {
//                System.out.println("customOnScrollStateChanged 2 listener");
            }
        });

        // Perform scrolling to position
        new Handler().post(new Runnable() {
            @Override
            public void run() {
                view.smoothScrollToPositionFromTop(position, 0);
            }
        });
    }

    private static View getChildAtPosition(final AdapterView view, final int position) {
        final int index = position - view.getFirstVisiblePosition();
        if ((index >= 0) && (index < view.getChildCount())) {
            return view.getChildAt(index);
        } else {
            return null;
        }
    }


}
