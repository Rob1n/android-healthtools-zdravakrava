package hr.shms.zdravakravaalati.Fragments;



import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import hr.shms.zdravakravaalati.Entities.UserClass;
import hr.shms.zdravakravaalati.R;
import hr.shms.zdravakravaalati.Util.Application;


public class Fragment4BodyShape extends Fragment {
    public Fragment4BodyShape() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_4_body_shape, container, false);
        UserClass korisnik = Application.getmInstance().getKorisnik();

        TextView waistTextView = (TextView) view.findViewById(R.id.frag4_waistTextView);
        TextView hipsTextView = (TextView) view.findViewById(R.id.frag4_hipsTextView);

        TextView waistTextCounter = (TextView) view.findViewById(R.id.frag4_waistTextCounter);
        TextView hipsTextCounter = (TextView) view.findViewById(R.id.frag4_hipsTextCounter);

        waistTextView.setText(getResources().getString(R.string.waist)+":");
        hipsTextView.setText(getResources().getString(R.string.hips)+":");

        switch (korisnik.getMeasurementSystem()){
            case METRIC:
                waistTextCounter.setText(Integer.toString(korisnik.getWrist())+" cm");
                hipsTextCounter.setText(Integer.toString(korisnik.getHips())+" cm");
                break;
            case IMPERIAL:
                waistTextCounter.setText(Integer.toString(korisnik.getWrist())+"\"");
                hipsTextCounter.setText(Integer.toString(korisnik.getHips())+"\"");
                break;
        }


        return view;
    }


}
