package hr.shms.zdravakravaalati.Fragments;


import android.app.FragmentManager;
import android.content.Context;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import hr.shms.zdravakravaalati.Enums.Gender;
import hr.shms.zdravakravaalati.R;
import hr.shms.zdravakravaalati.Util.Application;
import hr.shms.zdravakravaalati.Entities.UserClass;

enum AAction {
    ENLARGE_LEFT, ENLARGE_RIGHT, SHRINK_LEFT, SHRINK_RIGHT, SETUP_L_LEFT, SETUP_L_RIGHT, SETUP_R_LEFT, SETUP_R_RIGHT
}

public class Setup1Gender extends Fragment {
    public Setup1Gender() {}

    private Context applicationContext;
    private UserClass korisnik;

    private ImageView rightImageView;
    private ImageView leftImageView;

    /* //////////////////////////////////
       OVERRIDE METHODS
    */ //////////////////////////////////

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.setup1_gender, container, false);
        applicationContext = getActivity().getApplicationContext();
        korisnik = Application.getmInstance().getKorisnik();

        ImageView btn_next = (ImageView) view.findViewById(R.id.btn_setup1_next);
        rightImageView = (ImageView) view.findViewById(R.id.imageView_setup1_man);
        leftImageView = (ImageView) view.findViewById(R.id.imageView_setup1_woman);

        btn_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Fragment fragment = new Setup2Height();
                FragmentManager fragmentManager = getFragmentManager();
                fragmentManager.beginTransaction()
                        .setCustomAnimations(R.animator.card_flip_right_in, R.animator.card_flip_right_out,
                                R.animator.card_flip_left_in, R.animator.card_flip_left_out)
                        .replace(R.id.main_container, fragment)
                        .addToBackStack(null)
                        .commit();
            }
        });

        // init
        switch (korisnik.getGender()){
            case MALE:
                animImage(leftImageView, AAction.SETUP_R_LEFT);
                animImage(rightImageView, AAction.SETUP_R_RIGHT);
                break;
            case FEMALE:
                animImage(leftImageView, AAction.SETUP_L_LEFT);
                animImage(rightImageView, AAction.SETUP_L_RIGHT);
                break;
        }

        // image on click listeners
        leftImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (korisnik.getGender().equals(Gender.MALE)) {
                    animImage(leftImageView, AAction.ENLARGE_LEFT);
                    animImage(rightImageView, AAction.SHRINK_RIGHT);
                }
                korisnik.setGender(Gender.FEMALE);
            }
        });

        rightImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (korisnik.getGender().equals(Gender.FEMALE)) {
                    animImage(leftImageView, AAction.SHRINK_LEFT);
                    animImage(rightImageView, AAction.ENLARGE_RIGHT);
                }
                korisnik.setGender(Gender.MALE);
            }
        });

        return view;
    }

    /* //////////////////////////////////
       PRIVATE METHODS
    */ //////////////////////////////////

    /**
     * SETUP_* type animations initialize the image - from normal size (d=0) to max or min size <br/>
     * SETUP_L_* applies to the image on the left, SETUP_R_* apply to the image on the right  <br/>
     * SETUP_*_L move the image to the left, SETUP_*_R move it to the right <br/>
     * ENLARGE_* type animations resize from min size to max size  <br/>
     * SHRINK_* type animations resize from max size to min size  <br/>
     * @param imageView imageView on which the animation is applied
     * @param selectedAnimation enum type of animation that is to be performed
     */
    private void animImage (ImageView imageView, AAction selectedAnimation){
        Animation animation = null;
        switch (selectedAnimation){
            case ENLARGE_LEFT:
                animation = AnimationUtils.loadAnimation(applicationContext, R.anim.enlarge_left); break;
            case ENLARGE_RIGHT:
                animation = AnimationUtils.loadAnimation(applicationContext, R.anim.enlarge_right); break;
            case SHRINK_LEFT:
                animation = AnimationUtils.loadAnimation(applicationContext, R.anim.shrink_left); break;
            case SHRINK_RIGHT:
                animation = AnimationUtils.loadAnimation(applicationContext, R.anim.shrink_right); break;
            case SETUP_L_LEFT:
                animation = AnimationUtils.loadAnimation(applicationContext, R.anim.setup_l_left); break;
            case SETUP_L_RIGHT:
                animation = AnimationUtils.loadAnimation(applicationContext, R.anim.setup_l_right); break;
            case SETUP_R_LEFT:
                animation = AnimationUtils.loadAnimation(applicationContext, R.anim.setup_r_left); break;
            case SETUP_R_RIGHT:
                animation = AnimationUtils.loadAnimation(applicationContext, R.anim.setup_r_right); break;
        }
        if (animation != null) {
            animation.setFillAfter(true); // animation persists after it's finished
            imageView.startAnimation(animation);
        }
    }


}