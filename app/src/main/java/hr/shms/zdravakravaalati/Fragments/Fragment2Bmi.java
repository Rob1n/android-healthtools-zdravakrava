package hr.shms.zdravakravaalati.Fragments;

import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;


import hr.shms.zdravakravaalati.Dialogs.Frag2Info;
import hr.shms.zdravakravaalati.Util.Application;
import hr.shms.zdravakravaalati.R;
import hr.shms.zdravakravaalati.Entities.UserClass;


public class Fragment2Bmi extends Fragment {
    public Fragment2Bmi(){}

    private Button btn_explanation;

    private TextView bmiCounterTextView;
    private TextView bmiClassTextView;
    private TextView descriptionTextView;
    private TextView suggestionTextView;

    UserClass korisnik;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState){

        View view = inflater.inflate(R.layout.fragment_2_bmi, container, false);
        korisnik = Application.getmInstance().getKorisnik();

        // VIEWS
        btn_explanation = (Button) view.findViewById(R.id.frag2_btn_explanation);

        bmiCounterTextView = (TextView) view.findViewById(R.id.frag2_bmi_counter);
        bmiClassTextView = (TextView) view.findViewById(R.id.frag2_bmi_class);
        descriptionTextView = (TextView) view.findViewById(R.id.frag2_description);
        suggestionTextView = (TextView) view.findViewById(R.id.frag2_suggestion);

        // SETTING TEXT
        bmiCounterTextView.setText(korisnik.getBmiText());
        bmiCounterTextView.setTextColor(korisnik.getBmiColor());

        bmiClassTextView.setTextColor(getResources().getColor(R.color.white));
        bmiClassTextView.setBackgroundColor(korisnik.getBmiColor());

        String[] typeDescription = null;
        switch (korisnik.getBmiClass()){
            case 1: typeDescription = getResources().getStringArray(R.array.bmi_type1); break;
            case 2: typeDescription = getResources().getStringArray(R.array.bmi_type2); break;
            case 3: typeDescription = getResources().getStringArray(R.array.bmi_type3); break;
            case 4: typeDescription = getResources().getStringArray(R.array.bmi_type4); break;
            case 5: typeDescription = getResources().getStringArray(R.array.bmi_type5); break;
            case 6: typeDescription = getResources().getStringArray(R.array.bmi_type6); break;
            case 7: typeDescription = getResources().getStringArray(R.array.bmi_type7); break;
            case 8: typeDescription = getResources().getStringArray(R.array.bmi_type8); break;
        }
        assert typeDescription != null;
        bmiClassTextView.setText(typeDescription[0]);
        descriptionTextView.setText(typeDescription[1]);
        suggestionTextView.setText(typeDescription[2]);


        // ON CLICK LISTENERS
        btn_explanation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new Frag2Info().show(getFragmentManager(), "Frag2Info");
            }
        });

        return view;
    }


}
