package hr.shms.zdravakravaalati.Fragments;



import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;

import hr.shms.zdravakravaalati.Activities.MainActivity;
import hr.shms.zdravakravaalati.Adapters.OverviewListAdapter;
import hr.shms.zdravakravaalati.Entities.ItemOverviewListView;
import hr.shms.zdravakravaalati.R;
import hr.shms.zdravakravaalati.Entities.UserClass;
import hr.shms.zdravakravaalati.Util.Application;


public class SetupOverview extends Fragment {
    public SetupOverview() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.setup_overview, container, false);
        UserClass korisnik = Application.getmInstance().getKorisnik();

        // INIT VIEWS
        ListView overviewListView = (ListView) view.findViewById(R.id.setup_overview_listView);

        final ArrayList<ItemOverviewListView> dataList = new ArrayList<ItemOverviewListView>();

        // BMI
        dataList.add(new ItemOverviewListView(0, getString(R.string.body_mass_index), korisnik.calculateBmi(), true));

        // MEASUREMENT UNIT SENSITIVE DATA - IBW
        switch (korisnik.getMeasurementSystem()){
            case METRIC:
                dataList.add(new ItemOverviewListView(1, getString(R.string.ideal_body_weight), korisnik.calculateIbw(), "kg", true));
                break;
            case IMPERIAL:
                dataList.add(new ItemOverviewListView(1, getString(R.string.ideal_body_weight), korisnik.calculateIbw(), "lb", true));
                break;
        }
        // ADDING ADJUSTED IBW IF NECCESSARY
//        if (korisnik.getWeight() > korisnik.getIbw()*1.3 ){
//            switch (korisnik.getMeasurementSystem()){
//                case METRIC:
//                    dataList.add(new ItemOverviewListView(1, getString(R.string.adjusted_ideal_body_weight), korisnik.calculateAIbw(), "kg", true));
//                    break;
//                case IMPERIAL:
//                    dataList.add(new ItemOverviewListView(1, getString(R.string.adjusted_ideal_body_weight), korisnik.calculateAIbw(), "lb", true));
//                    break;
//            }
//
//        }

        // BODY SHAPE
        dataList.add(new ItemOverviewListView(2, getString(R.string.body_shape), korisnik.calculateBodyShape(), true));

        // BODY BUILD
        dataList.add(new ItemOverviewListView(3, getString(R.string.body_build), korisnik.calculateBodyBuild(), true));

        // SETTING ADAPTER
        OverviewListAdapter overviewListAdapter = new OverviewListAdapter(getActivity(), 0, dataList);
        overviewListView.setAdapter(overviewListAdapter);

        // ON CLICK LISTENER
        overviewListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Fragment fragment = null;
                switch (dataList.get(i).getId()) {
                    case 0: // BMI
                        fragment = new Fragment2Bmi();
                        break;
                    case 1: // IBW
                        fragment = new Fragment3Ibw();
                        break;
                    case 2: // BODY SHAPE
                        fragment = new Fragment4BodyShape();
                        break;
                    case 3: // BODY BUILD
                        fragment = new Fragment5BodyBuild();
                        break;
                }

                getFragmentManager().beginTransaction()
                        .replace(R.id.main_container, fragment)
                        .addToBackStack(null)
                        .commit();

                // simulates the click on the navigation drawer (currently selected item is lighter)
                // +1 is due to the fact that the list doesn't have the first, setup wizard, entry
                ((MainActivity) getActivity()).getmDrawerList().setItemChecked(dataList.get(i).getId() + 1, true);
                ((MainActivity) getActivity()).getmDrawerList().setSelection(dataList.get(i).getId() + 1);

            }
        });

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        // after returning to overview, navdrawer highlight needs to be reset too
        ((MainActivity) getActivity()).getmDrawerList().setItemChecked(0, true);
        ((MainActivity) getActivity()).getmDrawerList().setSelection(0);
    }
}
