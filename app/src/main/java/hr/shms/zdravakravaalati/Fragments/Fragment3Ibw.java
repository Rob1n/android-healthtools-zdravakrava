package hr.shms.zdravakravaalati.Fragments;



import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import hr.shms.zdravakravaalati.Entities.UserClass;
import hr.shms.zdravakravaalati.R;
import hr.shms.zdravakravaalati.Util.Application;


public class Fragment3Ibw extends Fragment {
    public Fragment3Ibw() {}

    TextView ibwTextView;
    TextView weightTextView;

    UserClass korisnik;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_3_ibw, container, false);

        korisnik = Application.getmInstance().getKorisnik();

        ibwTextView = (TextView) view.findViewById(R.id.frag3_ibw_value);
        weightTextView = (TextView) view.findViewById(R.id.frag3_current_weight);

        ibwTextView.setText(Double.toString(korisnik.getIbw()));
        weightTextView.setText(Double.toString(korisnik.getWeight()));


        return view;
    }




}