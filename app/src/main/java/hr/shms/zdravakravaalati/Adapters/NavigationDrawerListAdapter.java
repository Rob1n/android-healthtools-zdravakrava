package hr.shms.zdravakravaalati.Adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import hr.shms.zdravakravaalati.R;


public class NavigationDrawerListAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<String> navDrawerItems;

    public NavigationDrawerListAdapter(Context context, ArrayList<String> items){
        this.context = context;
        this.navDrawerItems = items;
    }

    @Override
    public int getCount() {
        return navDrawerItems.size();
    }

    @Override
    public Object getItem(int i) {
        return navDrawerItems.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {

        if (view == null){
            LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            view = mInflater.inflate(R.layout.lv_item_navigation_drawer, null);
        }

        TextView text = (TextView) view.findViewById(R.id.nav_drawer_item_text);
        text.setText(navDrawerItems.get(position));

        return view;
    }
}
