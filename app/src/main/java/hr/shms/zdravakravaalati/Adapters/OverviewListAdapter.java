package hr.shms.zdravakravaalati.Adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import hr.shms.zdravakravaalati.Entities.ItemOverviewListView;
import hr.shms.zdravakravaalati.R;
import hr.shms.zdravakravaalati.Util.Functions;


public class OverviewListAdapter extends ArrayAdapter<ItemOverviewListView> {

    private Context context;
    private ArrayList<ItemOverviewListView> dataList;


    public OverviewListAdapter(Context context, int resource, ArrayList<ItemOverviewListView> objects) {
        super(context, resource, objects);
        this.context = context;
        this.dataList = objects;
    }


    @Override
    public int getCount() {
        return dataList.size();
    }

    @Override
    public ItemOverviewListView getItem(int position) {
        return dataList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View view, ViewGroup viewGroup) {

        if (view == null){
            LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            view = mInflater.inflate(R.layout.lv_item_overview, null);
        }

        // CURRENT ITEM
        final ItemOverviewListView item = getItem(position);

        TextView parameterTextView = (TextView) view.findViewById(R.id.overview_item_textView);
        View layoutView = view.findViewById(R.id.overview_item_horLayout);
        String tekst = "";
        switch (item.getId()){
            case 0: // BMI
                tekst = item.getName()+": "+item.getValue();
                break;
            case 1: // IBW
                tekst = item.getName()+": "+ Functions.roundDoubleToInt(item.getValue()) + " " + item.getmUnit();
                break;
            case 2: // BODY SHAPE
                tekst = item.getName()+": "+ item.getStringValue();
                break;
            case 3: // BODY BUILD
                tekst = item.getName()+": "+ item.getStringValue();
                break;
        }
        parameterTextView.setText(tekst);

        ImageView moreInfoImageView = (ImageView) view.findViewById(R.id.overview_item_imageView);
        if (item.isShowImage())
            moreInfoImageView.setVisibility(View.VISIBLE);
        else
            moreInfoImageView.setVisibility(View.GONE);

        notifyDataSetChanged();
        return view;
    }
}
