package hr.shms.zdravakravaalati.Adapters;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import hr.shms.zdravakravaalati.R;
import hr.shms.zdravakravaalati.Util.Application;
import hr.shms.zdravakravaalati.Enums.MeasurementUnit;


public class HeightListAdapter extends ArrayAdapter<Integer> {

    private Context context;
    private ArrayList<Integer> integerArrayList;

    SharedPreferences sharedPreferences;
    MeasurementUnit measurementUnitSystem;

    MeasurementUnit displayMode;

    public HeightListAdapter(Context context, int textViewResourceId, ArrayList<Integer> objects, MeasurementUnit displayMode) {
        super( context, textViewResourceId, objects );
        this.context = context;
        integerArrayList = objects;
        this.displayMode = displayMode;
        sharedPreferences = Application.getmInstance().getSharedPreferencesFromApp();
        measurementUnitSystem = MeasurementUnit.toMyEnum(sharedPreferences.getString("measurementSystem", MeasurementUnit.METRIC.toString()));
    }

    public HeightListAdapter(Context context, int textViewResourceId, ArrayList<Integer> objects) {
        super( context, textViewResourceId, objects );
        this.context = context;
        integerArrayList = objects;
        sharedPreferences = Application.getmInstance().getSharedPreferencesFromApp();
        measurementUnitSystem = MeasurementUnit.toMyEnum(sharedPreferences.getString("measurementSystem", MeasurementUnit.METRIC.toString()));
        this.displayMode = measurementUnitSystem;
    }

    @Override
    public int getCount() {
        return integerArrayList.size();
    }

    @Override
    public Integer getItem(int i) {
        return integerArrayList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        if (view == null){
            LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);

            switch (displayMode){
                case METRIC:
                    view = mInflater.inflate(R.layout.lv_item_ver_cm, null);
                    break;
                case IMPERIAL:
                    view = mInflater.inflate(R.layout.lv_item_ver_inch, null);
                    break;
            }

        }
        // prikaz trenutne vrijednosti
        switch (displayMode){
            case METRIC:
                TextView iznos_cm = (TextView) view.findViewById(R.id.wiz_1_height_count);
                iznos_cm.setText(Integer.toString(getItem(i)));
                break;
            case IMPERIAL:
                TextView iznos_inch = (TextView) view.findViewById(R.id.textView_item_ver_inch);
                TextView iznos_feet = (TextView) view.findViewById(R.id.textView_item_ver_feet);
                int inches = getItem(i);
                int feet = inches/12;
                inches -= feet*12;
                iznos_feet.setText(Integer.toString(feet)+"\'");
                iznos_inch.setText(Integer.toString(inches)+"\"");
                break;
        }

        notifyDataSetChanged();
        return view;
    }

    @Override
    public boolean isEnabled(int position) {
        return false;
    }



}
