package hr.shms.zdravakravaalati.Enums;

/**
* Created by tomislav on 9/10/14.
*/
public enum MeasurementUnit {
    METRIC, IMPERIAL;

    public static MeasurementUnit toMyEnum (String myEnumString) {
        try {
            return MeasurementUnit.valueOf(myEnumString);
        } catch (Exception ex) {
            return METRIC;
        }
    }
}
