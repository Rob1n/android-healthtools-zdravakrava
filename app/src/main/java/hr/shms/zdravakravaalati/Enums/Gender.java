package hr.shms.zdravakravaalati.Enums;

/**
* Created by tomislav on 9/10/14.
*/
public enum Gender {
    MALE, FEMALE;

    public static Gender toMyEnum (String myEnumString) {
        try {
            return Gender.valueOf(myEnumString);
        } catch (Exception ex) {
            return MALE;
        }
    }
}
