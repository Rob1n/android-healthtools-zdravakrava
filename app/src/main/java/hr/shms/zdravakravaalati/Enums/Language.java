package hr.shms.zdravakravaalati.Enums;


public enum Language {
    ENGLISH, CROATIAN;

    public static Language toMyEnum (String myEnumString) {
        try {
            return Language.valueOf(myEnumString);
        } catch (Exception ex) {
            return ENGLISH;
        }
    }

}
