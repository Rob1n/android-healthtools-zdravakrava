# Health Tools - Zdrava Krava app

Target wireframe: https://docs.google.com/presentation/d/1y4PbsaX-5PConLsJ3nkr-DHxIBAWIDPav9wkoqQ60T4/edit?usp=sharing

Stuff to do: 

    Actionbar:
        - system of measurement - working but name too long
        - languages - translations incomplete
        - notifications - working
        - feedback - modify info (recipient data)
        - about app - possibly add a link to the same activity as from overview

    General:
        - navigation drawer icons?
        - formulas from zdravakrava tools 
                - body mass index - for adults and children
                - ideal body weight - multiple formulas or the one from zdravakrava.hr
                - adjusted body weight
                - body shape - hip to waist ratio? need formula and appropriate description and meaning
                - body build - male and female body types? need formulas and appropriate descriptions and meaning
        - min/max values for measurements
        - action bar / navigation drawer / overflow icons required
        - fonts?
        - color scheme
        - new slider item images
        - images for the stick(wo)man avatar

    Setup wizard :
            - missing age selection step - or at least adult toggle (possibly same calculations for everyone 18+)
            

    Hierarchy problems - need a structure
    Fragment 1 content always opened first? His location in the settings bar or the navigation drawer?
    
    Drawers 2-5 content questionable, need images, text or more formulas
    
    Drawer 2 BMI - color grading
    
    Drawer 3 IBW - 5 different formulas to display
                 - adjusted IBW display
                 
    Drawer 4 body shape content? need formulas, flavor text and images
     
    Drawer 5 body build content? need formulas, flavor text and images

![diagram](/app/src/main/res/drawable/diagram.png)

